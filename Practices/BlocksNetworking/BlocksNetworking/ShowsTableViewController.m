//
//  MainTableViewController.m
//  Models
//
//  Created by Daniel García on 17/05/14.
//  Copyright (c) 2014 Produkt. All rights reserved.
//

#import "ShowsTableViewController.h"
#import "Show.h"
#import "NSString+Random.h"
#import "BlockButtonItem.h"
#import "ShowDetailViewController.h"
#import "ShowsProvider.h"
#import "ReachabilityService.h"

static NSString * const savedShowsFileName=@"shows.plist";

@interface ShowsTableViewController ()
@property (strong,nonatomic) BlockButtonItem *addShowButton;
@property (strong,nonatomic) NSMutableArray *shows;
@property (strong,nonatomic) ShowsProvider *showsProvider;
@end

@implementation ShowsTableViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if(self){
        self.title=@"Shows";
        _shows=[NSMutableArray array];
        _showsProvider=[[ShowsProvider alloc] init];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(connectionBecameAvailable:) name:ReachabilityServiceChangeAvailable object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(connectionBecameUnavailable:) name:ReachabilityServiceChangeNotAvailable object:nil];
    }
    return self;
}

#pragma mark - Reachability
- (void)connectionBecameAvailable:(NSNotification *)notification{
    NSLog(@"connectionBecameAvailable");
}
- (void)connectionBecameUnavailable:(NSNotification *)notification{
    NSLog(@"connectionBecameUnavailable");
}

#pragma mark - Views lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIBarButtonItem *saveShowsButton=[[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStylePlain target:self action:@selector(saveShows:)];
    self.navigationItem.leftBarButtonItem = saveShowsButton;
    
    
    self.addShowButton=[[BlockButtonItem alloc] initWithTitle:@"Add" block:^{
        [self.shows addObject:[self randomShow]];
        [self.tableView reloadData];
    }];
    
    
    self.navigationItem.rightBarButtonItem = self.addShowButton;
    
    [self loadShows];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

- (Show *)randomShow{
    Show *show=[[Show alloc] init];
    show.showId=[NSString randomString];
    show.showTitle=[NSString randomString];
    show.showDescription=[NSString randomString];
    show.showRating=arc4random()/10.0f;
    show.posterImageURL=[NSURL URLWithString:@"http://thetvdb.com/banners/posters/76290-4.jpg"];
    return show;
}

- (NSString *)archivePath{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    return [documentsDirectory stringByAppendingPathComponent:savedShowsFileName];
}

- (void)saveShows:(id)sender{
    if (self.shows.count) {
        NSData *showsData=[NSKeyedArchiver archivedDataWithRootObject:self.shows];
        NSDictionary *showsSaveData=@{@"save date":[NSDate date],@"shows array":showsData};
        [showsSaveData writeToFile:[self archivePath] atomically:YES];
        NSLog(@"Saved File to %@",[self archivePath]);
    }
}

- (void)loadShows{
    @weakify(self);
    [self.showsProvider showsWithSuccessBlock:^(id data) {
        @strongify(self);
        self.shows=data;
        [self.tableView reloadData];
    } errorBlock:^(NSError *error) {
        
    }];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.shows.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"reuseIdentifier"];
    if (!cell) {
        cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"reuseIdentifier"];
    }
    Show *show=[self.shows objectAtIndex:indexPath.item];
    cell.textLabel.text=show.showTitle;
    cell.detailTextLabel.text=show.showDescription;
    return cell;
}

#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    Show *show=[self.shows objectAtIndex:indexPath.item];
    
    __block Show *selectedShow;
    [self.shows enumerateObjectsUsingBlock:^(Show *obj, NSUInteger idx, BOOL *stop) {
        if ([show isEqualToShow:obj]) {
            selectedShow=obj;
        }
    }];
    
    ShowDetailViewController *detailViewController=[[ShowDetailViewController alloc] initWithShow:selectedShow];
    [self.navigationController pushViewController:detailViewController animated:YES];
}
@end
